import React, { Component } from "react";
import { Link, useRouteMatch } from "react-router-dom";
import { connect } from "react-redux";
const Artist = (props) => {
  console.log("artist");
  console.log("dummy state ===>>", props);
  let match = useRouteMatch();
  console.log(match);
  return (
    <div>
      <h1>Hello Artist,</h1>
      <p>Stream Your Music Here, hello</p>
      <button onClick={props.onDummy}>Change State</button>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    dummy: state.dummy,
  };
};

const mapDispachToProps = (dispatch) => {
  return {
    onDummy: () => dispatch({ type: "DUMMY_ACTION", value: "CHANGED" }),
  };
};
export default connect(mapStateToProps, mapDispachToProps)(Artist);
// export default Artist;
