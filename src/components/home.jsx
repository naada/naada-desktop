import React, { Component } from "react";
import { Link } from "react-router-dom";
import routePath from "../constants/routes.json";

class Home extends Component {
  render() {
    console.log("home");
    return (
      <div>
        <h1>Home</h1>
        <h1>
          <Link to={routePath.ARTIST}>Artist</Link>
        </h1>
      </div>
    );
  }
}

export default Home;
